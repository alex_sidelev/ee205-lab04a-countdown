///////////////////////////////////////////////////////////////////////////////
//// University of Hawaii, College of Engineering
//// EE 205 - Object Oriented Programming
//// Lab 04a - Countdown
////
//// Usage:  countdown.h 
////
//// Result:
////   Header file for counts down (or towards) a significant date
////
//// Example:
////   
////
//// @author Alexander Sidelev <asidelev@hawaii.edu>
//// @date   02_02_2021
/////////////////////////////////////////////////////////////////////////////////

//delcare the fxn sleep which pauses for 1 sec
int sleep();

// Print the count fxn
void printCounter() ;


