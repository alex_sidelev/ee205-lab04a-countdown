///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down since a significant date
//
// Example:
//   @todo
//
// @author Alexander Sidelev <asidelev@hawaii.edu>
// @date   02_08_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "countdown.h"

int main(int argc, char* argv[]) {
   //set the refrence date to December 21st, 2012, i.e. End of the Mayan Calendar
   struct tm refTime;
   refTime.tm_year   =  2012 - 1900;
   refTime.tm_mon    =  12 - 1;
   refTime.tm_mday   =  21;
   refTime.tm_hour   =  0;
   refTime.tm_min    =  0;
   refTime.tm_sec    =  0;
   refTime.tm_wday   =  5;
  // Print out reference date
   printf("Refrence Time: %s\n", asctime(&refTime));

   // seconds in a time unit constants
   const long int secYear  =  3.154e+7;
   const long int secDay   =  86400;
   const long int secHour  =  3600;
   const long int secMin   =  60;

   //infinite while-loop to countdown, to countdown towards flip values in elapTime
   while(1){
      //get the most current time
      time_t now;
      time(&now);
      time_t refTimeUnix;
   //convert broken-down time to time since Epoch
      refTimeUnix  =  mktime(&refTime);
   //difference between now and reference date
      long elapTime = difftime(now, refTimeUnix);

   //math conversions of elapsed time to years, days, hours, minutes, seconds
      long Y = ((difftime(now, refTimeUnix))/secYear);
      long D = (elapTime%secYear)/secDay;
      long H = ((elapTime%secYear)%secDay)/secHour;
      long M = ((((elapTime%secYear)%secDay)%secHour)/secMin);
      long S = ((((elapTime%secYear)%secDay)%secHour)%secMin);
      printf("Years: %ld   Days: %ld  Hours:%ld  Minutes: %ld  Seconds: %ld\n", (Y),(D),(H),(M),(S));
   
   //fxn that pauses for 1 second
      sleep(1);
      };

   return 0;
}
